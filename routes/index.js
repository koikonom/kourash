var  malakia = require('../malakia.js');

/*
 * GET home page.
 */

exports.index = function(req, res){
  phrase = malakia.generate_phrase();
  tweet_prefix = "Νεα τρομοκρατική οργάνωση: ";
  res.render('index', { title: phrase, tweet_prefix:tweet_prefix });
};